---?color=linear-gradient(to right, #963e36, #963e36)
@title[Introduction]

@snap[west headline text-white span-70]
Kanban
@snapend 

@snap[south]
a method that shows us how our work works
@snapend
---
+++?image=template/img/bg/red.jpg&position=left&size=30% 100%
@title[Cose porta]

@snap[west text-white]
@size[3em](1.)
@snapend

@snap[east span-70]
It brings us a shared understanding of the work we do, including the rules by which we do the work, how much we can handle at a time, and how well we deliver work to our internal and external customers.
@snapend

+++?image=template/img/bg/red.jpg&position=left&size=30% 100%
@title[Cose porta]

@snap[west text-white]
@size[3em](2.)
@snapend

@snap[east span-70]
Once we achieve this understanding, we can start to improve. We can become more predictable and work at a @css[text-blue](more sustainable pace)
@snapend

+++?image=template/img/bg/red.jpg&position=right&size=50% 100%
@title[Storia]

@snap[west split-screen-heading text-orange span-50]
Storia
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Lean e la Toyota Motor System
- David Anderson riadatta il modello per lo sviluppo di software e servizi (2010)
@olend
@snapend

+++?image=template/img/bg/red.jpg&position=right&size=50% 100%
@title[Pillars]

@snap[west split-screen-heading text-orange span-50]
Pillars
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Approccio "Parti da dove sei adessso"
- The Kanban Method is based on making visible what is otherwise intangible knowledge work,
- Customer satisfaction oriented
@olend
@snapend

+++?image=assets/rowing.jpg&position=left&size=100% auto
@title[Doing it wrong]

@snap[north]
@quote[Comprendi i principi e poi applica le pratiche. <br> Non **viceversa**]
@snapend

 

+++?color=linear-gradient(to top, #963e36, #963d35)
@title[Respect]

@snap[midpoint announce-coming-soon text-white]
RESPECT
@snapend

@snap[south text-white]
il valore fondante
@snapend


+++?image=template/img/bg/red.jpg&position=left&size=70% 100%
@title[I principi]

@snap[east split-screen-heading text-red span-30]
VALORI
@snapend

@snap[west text-white span-65]
@ul[split-screen-list](false)
- **Transparency** The belief that sharing information openly improves the flow of business value. Using clear and straightforward vocabulary is part of this value.
- **Balance** The understanding that different aspects, viewpoints, and capabilities all must be balanced for effectiveness. 
- **Collaboration** Working together. 
- **Customer Focus** Knowing the goal for the system. Every kanban system flows to a point of realizing value—when customers receive a required item or service. 
- **Flow** The realization that work is a flow of value, whether continuous or episodic. Seeing flow is an essential starting point in using Kanban. 
- **Leadership** The ability to inspire others to action through example, words, and reflection. 
- **Understanding** Primarily self-knowledge (both of the individual and of the organization) in order to move forward. 
- **Agreement** The commitment to move together toward goals, respecting—and where possible
- **Respect** Valuing, understanding, and showing consideration for people.  
@ulend
@snapend


---
@title[Tip! Fullscreen]

![TIP](template/img/tip.png)
<br>
For the best viewing experience, press F for fullscreen.

---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---
@title[The Template Docs]

@snap[west headline span-100]
GitPitch<br>*The Template @css[text-orange](End) ;)*
@snapend

@snap[south docslink span-100]
For supporting documentation see the [The Template Docs](https://gitpitch.com/docs/the-template)
@snapend
